﻿using System;
using System.Linq;

namespace wealth_sim
{
    class Program
    {
        static void Main(string[] args)
        {
            const int numPeople = 100;
            const int startWealth = 100;

            
            int[] wealth = new int[numPeople];
            for (int i = 0; i < numPeople; i++)
                wealth[i] = startWealth;
            
            Random rand = new Random();
            int it = 0;
            while (it < 1000000)
            {
                it++;
                Console.WriteLine("Iteration:{0}", it);
                for (int p = 0; p < numPeople; p++)
                {
                    if (wealth[p] > 0)
                    {
                        // Select a person at random (note: can include self)
                        int giveTo = rand.Next(numPeople);
                        wealth[p] = wealth[p] - 1;
                        wealth[giveTo] = wealth[giveTo] + 1;
                    }
                }
            }

            Console.WriteLine("Max wealth: {0}", wealth.Max());
            Console.ReadLine();
        }
    }
}
